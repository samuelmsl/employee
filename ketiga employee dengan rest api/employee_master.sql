-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Jul 2020 pada 10.34
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `karyawan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `employee_master`
--

CREATE TABLE `employee_master` (
  `id` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `bd` varchar(255) DEFAULT NULL,
  `citizenship` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `nik` varchar(255) DEFAULT NULL,
  `pob` varchar(255) DEFAULT NULL,
  `profession` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `employee_master`
--

INSERT INTO `employee_master` (`id`, `address`, `bd`, `citizenship`, `gender`, `name`, `nik`, `pob`, `profession`, `religion`) VALUES
(14, 'Alloggio', '1 Januari 1996', 'WNI', 'perempuan', 'Ka Ocep lucu', '0874182184712', 'Semarang', 'Programmer', 'Katholik'),
(6, 'grandporis', '17 Maret 2000', 'WNI', 'Laki-laki', 'illix', '1234432112344321', 'jakarta', 'Programmer', 'Kristen'),
(7, 'Ketapang', '18 september 2002', 'WNI', 'Laki-laki', 'samuel', '1234432112344124', 'tangerang', 'Programmer', 'Kristen');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `employee_master`
--
ALTER TABLE `employee_master`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
