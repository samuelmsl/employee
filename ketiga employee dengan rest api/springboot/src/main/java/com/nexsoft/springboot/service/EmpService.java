package com.nexsoft.springboot.service;

import com.nexsoft.springboot.entity.Employee;
import com.nexsoft.springboot.repository.EmpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmpService {
    @Autowired
    private EmpRepository repository;

    public Employee saveEmployee(Employee employee) {
        return repository.save(employee);
    }

    public List<Employee> saveEmployees(List<Employee> employees){
        return repository.saveAll(employees);
    }

    public List<Employee> getEmployees() {
        return repository.findAll();
    }

    public Employee getEmployeeById(int id){
        return repository.findById(id).orElse(null);
    }

    public Employee getEmployeeByName(String name) {
        return repository.findByName(name);
    }

    public String deleteEmployee(int id) {
        repository.deleteById(id);
        return "Employee removed!!";
    }

//    public Employee updateEmployee(Employee employee) {
//        Employee existingEmployee = repository.findById(employee.getId()).orElse(null);
//        existingEmployee.set(employee.getName());
//        existingEmployee.setNik(employee.getNik());
//        existingEmployee.setpob(employee.getpob());
//        return repository.save(existingEmployee);
//    }
}