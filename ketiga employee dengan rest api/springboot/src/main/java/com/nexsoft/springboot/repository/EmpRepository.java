package com.nexsoft.springboot.repository;

import com.nexsoft.springboot.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmpRepository extends JpaRepository<Employee, Integer> {
    Employee findByName(String name);
}